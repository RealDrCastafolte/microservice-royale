package msroyale


import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import scala.io.StdIn

object WebServer {
  def main(args: Array[String]) {

    implicit val system = ActorSystem("my-system")
    implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher

    val route =
      path("healthcheck") {
        get {
          complete(HttpEntity(ContentTypes.`application/json`, """{"State":"OK"}"""))
        }
      }

    Http().bindAndHandle(route, "0.0.0.0", 9000)

  }
}